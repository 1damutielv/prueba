package ejercicios;

import java.util.Calendar;
import java.util.GregorianCalendar;

import utilidades.Entrada;

public class Ej5 {

	public static void main(String[] args) {
		GregorianCalendar gc = new GregorianCalendar();
		gc.setLenient(false);
		
		System.out.print("Día ? ");
		int dia = Entrada.entero();
		System.out.print("Mes (1-12)? ");
		int mes = Entrada.entero();
		System.out.print("Anio ? ");
		int anio = Entrada.entero();
		//Ajustamos el valor de cada campp del objeto gc
		gc.set(Calendar.DAY_OF_MONTH, dia);
		gc.set(Calendar.MONTH, mes-1); //GregorianCalendar numera los meses del 0 al 11
		gc.set(Calendar.YEAR, anio);
		gc.set(Calendar.HOUR_OF_DAY, 0);
		gc.set(Calendar.MINUTE, 0);
		gc.set(Calendar.SECOND, 0);
		
		System.out.println("Instante de partida: "+gc.getTime());
		
		String uni;
		boolean bien = false;
		do {
			System.out.print("Unidades (a,m,d,h,min,s,ms) ? ");
			uni = Entrada.cadena().toLowerCase();
			switch (uni) {
			case "a":
			case "m":
			case "d":
			case "h":
			case "min":
			case "s":
			case "ms":
				bien = true;
				break;
			default:
				bien = false;
			}
			if (!bien)
				System.out.println("La unidades admitidas son: a-años, m-meses, d-días, h-horas, min-minutos, s-segundos, ms-milisegundos");
		} while (!bien);
		
		System.out.print("Cantidad a desplazar ? ");
		int cant = Entrada.entero();
		
		GregorianCalendar nuevo = desplazarTiempo(gc, cant, uni);
		System.out.println("Tras aplicar desplazamiento: "+nuevo.getTime());
	} 
	public static GregorianCalendar desplazarTiempo(GregorianCalendar desde, int despl, String unidad) {
		int campo=0;
		switch (unidad) {
		case "a":
			campo=Calendar.YEAR;
			break;
		case "m":
			campo=Calendar.MONTH;
			break;
		case "d":
			campo=Calendar.DAY_OF_MONTH;
			break;
		case "h":
			campo=Calendar.HOUR_OF_DAY;
			break;
		case "min":
			campo=Calendar.MINUTE;
			break;
		case "s":
			campo=Calendar.SECOND;
			break;
		case "ms":
			campo=Calendar.MILLISECOND;
			break;
		default:
			throw new IllegalArgumentException("Unidad de tiempo incorrecta.");
		}
		//Creamos nuevo objeto GC y le asignamos el mismo instante de tiempo
		GregorianCalendar nuevo=new GregorianCalendar();
		nuevo.setTime(desde.getTime());
		
		nuevo.add(campo, despl);
		return nuevo;
	}
}
