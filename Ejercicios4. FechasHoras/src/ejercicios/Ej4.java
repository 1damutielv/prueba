package ejercicios;

import utilidades.Entrada;

public class Ej4 {

	public static void main(String[] args) throws InterruptedException {
		System.out.println("=== Cuenta atrás ===");
		int min=0;
		boolean bien = false;
		do {
			System.out.print("Minutos: ");
			try {
				min = Entrada.entero();
				bien = min>=0;
				if (!bien)
					System.out.println("Debe ser mayor o igual que 0");
			} catch (Exception e) {
				bien=false;
				System.out.println("Número incorrecto");
			}
		} while (!bien);
		
		int seg=0;
		bien = false;
		do {
			System.out.print("Segundos: ");
			try {
				seg = Entrada.entero();
				bien = seg>=0;
				if (!bien)
					System.out.println("Debe ser mayor o igual que 0");
			} catch (Exception e) {
				bien=false;
				System.out.println("Número incorrecto");
			}
		} while (!bien);
		
		int segRestantes=min*60+seg;
		
		while (segRestantes>=0){
			min=segRestantes/60;
			seg=segRestantes%60;
			System.out.printf("%d:%02d\n",min,seg);
			Thread.sleep(1000);
			segRestantes--;
		} 
		
		System.out.println("KIKIRIKI!!");
	} 
}
