package ejercicios;

import java.util.Calendar;
import java.util.GregorianCalendar;

import utilidades.Entrada;

public class Ej2 {

	public static void main(String[] args) {
		System.out.print("Día ? ");
		int dia = Entrada.entero();
		System.out.print("Mes (1-12)? ");
		int mes = Entrada.entero();
		System.out.print("Anio ? ");
		int anio = Entrada.entero();
		
		System.out.println(diaSemana(dia,mes,anio));
	} 
	
	private static String diaSemana(int dia, int mes, int anio) {
		String nombreDia=null;
		GregorianCalendar gc = new GregorianCalendar();
		gc.setLenient(false);
		//Ajustamos el valor de cada campo del objeto gc
		gc.set(Calendar.DAY_OF_MONTH, dia);
		gc.set(Calendar.MONTH, mes-1); //GregorianCalendar numera los meses del 0 al 11
		gc.set(Calendar.YEAR, anio);
		
		switch (gc.get(Calendar.DAY_OF_WEEK)) {
		case Calendar.MONDAY:
			nombreDia="lunes";
			break;
		case Calendar.TUESDAY:
			nombreDia="martes";
			break;
		case Calendar.WEDNESDAY:
			nombreDia="miércoles";
			break;
		case Calendar.THURSDAY:
			nombreDia="jueves";
			break;
		case Calendar.FRIDAY:
			nombreDia="viernes";
			break;
		case Calendar.SATURDAY:
			nombreDia="sábado";
			break;
		case Calendar.SUNDAY:
			nombreDia="domingo";
			break;
		}
		
		return nombreDia;
	}
}
