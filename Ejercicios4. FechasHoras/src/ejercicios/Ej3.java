package ejercicios;

import java.text.SimpleDateFormat;
import java.util.Date;

public class Ej3 {

	public static void main(String[] args) throws InterruptedException {
		SimpleDateFormat sdf=new SimpleDateFormat("HH:MM:ss");
		for(;;) {
			Date d=new Date();
			System.out.println(sdf.format(d));
			Thread.sleep(1000);
		}
	}

}
